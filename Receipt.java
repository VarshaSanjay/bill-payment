import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;
import java.math.*;
import java.text.DecimalFormat;

public class Receipt {

	private ArrayList<Product> productsList = new ArrayList<Product>();
	private double total;
	private double taxTotal;
	
	@SuppressWarnings("resource")
	public Receipt(String inputFileName){
		
		try {

            Scanner input = new Scanner(System.in);

            File file = new File(inputFileName);

            input = new Scanner(file);
            
            while (input.hasNextLine()) {
            	
            	String line = input.nextLine(); 
            	 
            	String[] words = line.split(" "); 
            	
            	int quantity = Integer.parseInt(words[0]); 
            	
            	boolean isImported = line.contains("imported"); 
            	
            	String[] exemptedItems =  new String[]{"book","chocolate","pills"}; 
            	
            	int exemptedItemIndex = containsItemFromArray(line,exemptedItems); 
            	
            	String exemptedType = null;
            	
            	if(exemptedItemIndex != -1){
            		
                	exemptedType = exemptedItems[exemptedItemIndex];
        			
            	}

            	int splitIndex = line.lastIndexOf("at");
            	
            	if(splitIndex == -1){
            		
            		System.out.println("Not good Format");
            		
            	} else {
            		
                	float price = Float.parseFloat((line.substring(splitIndex + 2))); 
                    
                	String name = line.substring(1, splitIndex); 
                	
                    for(int i = 0;i<quantity;i++){
                    	
                    	Product newProduct = null;
                    	
                    	if(isImported){
                    		
                        	if(exemptedType != null){
                        		
                        		if(exemptedType == "book"){
                        			newProduct = new Product(name,price,ItemList.IMPORTED_BOOK);
                        		} else if(exemptedType == "pills"){
                        			newProduct = new Product(name,price,ItemList.IMPORTED_MEDICAL);
                        		} else if(exemptedType == "chocolate"){
                        			newProduct = new Product(name,price,ItemList.IMPORTED_FOOD);
                        		}

                        	} else {
                        		newProduct = new Product(name,price,ItemList.IMPORTED_OTHERS);
                        	}
                        	
                    	} else {
                        	if(exemptedType != null){
                        		
                        		if(exemptedType == "book"){
                        			newProduct = new Product(name,price,ItemList.BOOK);
                        		} else if(exemptedType == "pills"){
                        			newProduct = new Product(name,price,ItemList.MEDICAL);
                        		} else if(exemptedType == "chocolate"){
                        			newProduct = new Product(name,price,ItemList.FOOD);
                        		}

                        	} else {
                        		newProduct = new Product(name,price,ItemList.OTHERS);
                        	}
                    	}
                    	
                        productsList.add(newProduct); 
                    }
            	}
            	
            }
            input.close();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
	}
	
	public void calculateTotals(){
		
		int numOfItems = productsList.size();
		
		BigDecimal receiptSum = new BigDecimal("0");
		BigDecimal receiptTax = new BigDecimal("0");
		
		for(int i = 0;i<numOfItems;i++){
			
			receiptTax = BigDecimal.valueOf(0);
			
			BigDecimal totalBeforeTax = new BigDecimal(String.valueOf(this.productsList.get(i).getPrice()));
			
			receiptSum = receiptSum.add(totalBeforeTax);
			
			if(productsList.get(i).isSalesTaxable()){
			
			    BigDecimal salesTaxPercent = new BigDecimal("0.10");
			    BigDecimal salesTax = salesTaxPercent.multiply(totalBeforeTax);
			    
			    salesTax = round(salesTax, BigDecimal.valueOf(0.05), RoundingMode.UP);
			    receiptTax = receiptTax.add(salesTax);
			    
    
			} 
			
			if(productsList.get(i).isImportedTaxable()){

			    BigDecimal importTaxPercent = new BigDecimal("0.05");
			    BigDecimal importTax = importTaxPercent.multiply(totalBeforeTax);
			    
			    importTax = round(importTax, BigDecimal.valueOf(0.05), RoundingMode.UP);
			    receiptTax = receiptTax.add(importTax);
			   
			}

			
			productsList.get(i).setPrice(receiptTax.floatValue() + productsList.get(i).getPrice());
		
			taxTotal += receiptTax.doubleValue();
			
			receiptSum = receiptSum.add(receiptTax);
		}
			
			taxTotal = roundTwoDecimals(taxTotal);
			total = receiptSum.doubleValue();
	}
	
	public void setTotal(BigDecimal amount){
		total = amount.doubleValue();
	}
	
	public double getTotal(){
		return total;
	}
	public void setSalesTaxTotal(BigDecimal amount){
		taxTotal = amount.doubleValue();
	}
	
	public double getSalesTaxTotal(){
		return taxTotal;
	}
	
	public static int containsItemFromArray(String inputString, String[] items) {
		
		int index = -1;
		
		for(int i = 0;i<items.length;i++){
			
			index = inputString.indexOf(items[i]);

			if(index != -1)
				return i;
				
		}
		return -1;
		
	}
	
	public static BigDecimal round(BigDecimal value, BigDecimal increment,RoundingMode roundingMode) {

		if (increment.signum() == 0) {
		return value;
		} else {
			BigDecimal divided = value.divide(increment, 0, roundingMode);
			BigDecimal result = divided.multiply(increment);
			result.setScale(2, RoundingMode.UNNECESSARY);
			return result;
		}
	}
	
	public double roundTwoDecimals(double d) {
	    DecimalFormat twoDForm = new DecimalFormat("#.##");
	    return Double.valueOf(twoDForm.format(d));
	}
	
	public void printReceipt(){
		
		int numOfItems = productsList.size();
		for(int i = 0;i<numOfItems;i++){
			System.out.println("1" + productsList.get(i).getName() + "at " + productsList.get(i).getPrice());
		}
		System.out.printf("Sales Tax: %.2f\n", taxTotal);
		System.out.println("Total: " + total);
	}
	
}