public class Main {

	public static void main(String[] args) {
		
		Receipt rep1 = new Receipt("input1.txt");	
		
		rep1.calculateTotals();
		
		System.out.println("Output 1");
		rep1.printReceipt();
		System.out.println();

		Receipt rep2 = new Receipt("input2.txt");

		rep2.calculateTotals();
		
		System.out.println("Output 2");
		rep2.printReceipt();
		System.out.println();
		
		Receipt rep3 = new Receipt("input3.txt");
		
		rep3.calculateTotals();
		
		System.out.println("Output 3");
		rep3.printReceipt();
		
	}

}